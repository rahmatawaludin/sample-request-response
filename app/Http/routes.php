<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    $request = app('Illuminate\Http\Request');
    return $request->method();
});

Route::get('/contact/about', function () {
    $request = app('Illuminate\Http\Request');
    return $request->path();
});

Route::any('/home', 'HomeController@index');

Route::get('registrasi', function() {
    return View::make('registrasi');
});
Route::post('registrasi', 'HomeController@registrasi');

Route::get('upload-form', function() {
    return View::make('upload_form');
});
Route::post('upload-profile-picture', 'HomeController@uploadProfilePicture');


Route::get('generate-cookie', 'HomeController@generateCookie');

Route::get('test-cookie', 'HomeController@testCookie');


Route::get('/post/memahami-laravel', function () {
    $request = app('Illuminate\Http\Request');
    // isi disini
    return $request->method();
});

Route::get('custom-header', function() {
    return (new Illuminate\Http\Response("Berhasil membuat custom header"))
                      ->header('Made-By', '@rahmatawaludin');
});

Route::get('post/buku-sublime-text', function() {
    return (new Illuminate\Http\Response("Buku masih dibuat", 404));
    // return response("Buku masih dibuat", 404);
});

Route::get('buku', function() {
    return redirect('http://leanpub.com/u/rahmatawaludin');
});

Route::get('daftar-buku', function() {
    return response()->json([
        ['judul' => 'Seminggu Belajar Laravel', 'url' => 'http://leanpub.com/seminggubelajarlaravel'],
        ['judul' => 'Menyelami Framework Laravel', 'url' => 'http://leanpub.com/bukularavel']
    ]);
});

Route::get('daftar-lagu', function() {
    return response()->json(['error' => 'Rahmat Awaludin bukan penyanyi.'], 403);
});

Route::get('download-kucing', function() {
    return response()->download(public_path() . '/img/kucing.gif');
});

Route::get('buku/menguasai-go-lang', function() {
    // return response()->jsonNotFound('Buku Go belum ditulis.');
    return response(json_encode(['error_message' => 'Buku Go belum ditulis.']), 404)
                    ->header('Content-Type', 'application/json');
});
